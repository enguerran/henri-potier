import React, { useState, useEffect } from 'react';
import { Box, Button, Text, Image } from 'grommet';

export const ProductList = ({ query, addProduct }) => {
  const [books, setBooks] = useState([]);

  useEffect(() => {
    const fetchBooks = async () => {
      // fetch
      const response = await fetch('http://henri-potier.xebia.fr/books');
      const books = await response.json();
      setBooks(books);
    };
    fetchBooks();
  }, []);

  return (
    <Box direction="row-responsive" wrap={true} justify="center" pad="medium">
      {books.map(book => {
        if (query) {
          const notMatchingQuery =
            book.title.toLowerCase().indexOf(query.toLowerCase()) < 0;
          if (notMatchingQuery) {
            return null;
          }
        }
        return (
          <Box
            key={book.isbn}
            width="medium"
            height="500px"
            margin="medium"
            background={{ color: 'light-2', opacity: 'strong' }}
            pad="large"
            justify="center"
            align="center"
            round
            gap="medium"
          >
            <Image src={book.cover} fit="contain" />
            <Text size="xlarge">{book.title}</Text>
            <Text size="large">{book.price} €</Text>
            <Button
              label="Ajouter au panier"
              onClick={() => addProduct(book)}
            />
          </Box>
        );
      })}
    </Box>
  );
};
