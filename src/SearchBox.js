import React from 'react';
import { Box, TextInput } from 'grommet';

export const SearchBox = ({ setQuery }) => {
  return (
    <Box margin="medium">
      <TextInput
        placeholder="Accio"
        onChange={event => setQuery(event.target.value)}
      />
    </Box>
  );
};
