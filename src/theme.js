export default {
  global: {
    colors: {
      brand: '#228BE6',
    },
    font: {
      family:
        "-apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', sans-serif",
      size: '15px',
      height: '20px',
    },
  },
};
