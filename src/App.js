import React, { useState } from 'react';
import {
  Box,
  Button,
  Collapsible,
  Heading,
  Grommet,
  Layer,
  ResponsiveContext,
} from 'grommet';
import { Cart as CartIcon, FormClose } from 'grommet-icons';
import theme from './theme';
import { ProductList } from './ProductList';
import { Cart } from './Cart';
import { AppBar } from './AppBar';
import { SearchBox } from './SearchBox';

const App = () => {
  const [cartProducts, setCartProducts] = useState([]);
  const [showSidebar, setShowSidebar] = useState(false);
  const [query, setQuery] = useState(null);

  return (
    <Grommet theme={theme} full>
      <ResponsiveContext.Consumer>
        {size => (
          <Box>
            <AppBar>
              <Heading level="3" margin="none">
                Henri Potier
              </Heading>
              <Button
                icon={<CartIcon />}
                onClick={() => setShowSidebar(!showSidebar)}
              />
            </AppBar>

            <Box direction="row" flex overflow={{ horizontal: 'hidden' }}>
              <Box flex>
                <SearchBox setQuery={setQuery} />
                <Box align="center" justify="center">
                  <ProductList
                    query={query}
                    addProduct={product => {
                      setCartProducts([...cartProducts, product]);
                      setShowSidebar(true);
                    }}
                  />
                </Box>
              </Box>

              {!showSidebar || size !== 'small' ? (
                <Collapsible direction="horizontal" open={showSidebar}>
                  <Box
                    width="medium"
                    background="light-2"
                    elevation="small"
                    align="center"
                    flex
                  >
                    <Cart products={cartProducts} />
                  </Box>
                </Collapsible>
              ) : (
                <Layer>
                  <Box
                    background="light-2"
                    tag="header"
                    justify="end"
                    align="center"
                    direction="row"
                  >
                    <Button
                      icon={<FormClose />}
                      onClick={() => setShowSidebar(false)}
                    />
                  </Box>
                  <Box
                    fill
                    background="light-2"
                    align="center"
                    justify="center"
                  >
                    <Cart products={cartProducts} />
                  </Box>
                </Layer>
              )}
            </Box>
          </Box>
        )}
      </ResponsiveContext.Consumer>
    </Grommet>
  );
};

export default App;
