import React, { useState, useEffect } from 'react';
import { Box, Button, Heading, Text, Image } from 'grommet';

const calculateOffer = ({ offer, total }) => ({
  percentage: () => {
    return total * (offer.value / 100);
  },
  minus: () => {
    return offer.value;
  },
  slice: () => {
    return (total / offer.sliceValue) * offer.value;
  },
});

const getBestDiscount = (offers, total) => {
  if (offers.length === 0) {
    return null;
  }

  return offers.reduce(
    (bestDiscount, offer) => {
      const discount = calculateOffer({ offer, total })[offer.type]();

      return discount > bestDiscount.discount
        ? { offer, discount }
        : bestDiscount;
    },
    { offer: null, discount: 0 }
  );
};

const offersTexts = {
  percentage: ({ value }) => `${value}% de réduction !`,
  minus: ({ value }) => `${value} € offerts !`,
  slice: ({ sliceValue, value }) =>
    `${value} € offerts tous les ${sliceValue} !`,
};

export const Cart = ({ products }) => {
  const [offers, setOffers] = useState([]);

  useEffect(() => {
    const productsIsbn = products.map(product => product.isbn);

    const fetchOffers = async () => {
      const response = await fetch(
        `http://henri-potier.xebia.fr/books/${productsIsbn.join(
          ','
        )}/commercialOffers`
      );
      const { offers } = await response.json();
      setOffers(offers);
    };
    if (productsIsbn.length > 0) {
      fetchOffers();
    }
  }, [products]);

  const total = products.reduce((acc, product) => acc + product.price, 0);
  const discount = getBestDiscount(offers, total);

  return (
    <>
      <Heading level="2">Panier</Heading>

      <Box direction="column" wrap={true} justify="center" pad="medium">
        {products.map((product, index) => (
          <Box
            key={index}
            height="250px"
            background="white"
            margin="small"
            pad="medium"
            justify="center"
            align="center"
            round
            gap="small"
          >
            <Image src={product.cover} fit="contain" />
            <Text>{product.title}</Text>
            <Text>{product.price} €</Text>
          </Box>
        ))}

        <Box margin={{ top: 'medium' }} gap="small">
          <Text>Total : {total} €</Text>
          {discount && (
            <Box>
              <Text weight="bold">
                {offersTexts[discount.offer.type](discount.offer)}
              </Text>
              {/* mmmm */}
              {discount.offer.type !== 'minus' && (
                <Text>Réduction : {discount.discount.toLocaleString()} €</Text>
              )}
              <Text weight="bold">
                Total : {(total - discount.discount).toLocaleString()} €
              </Text>
            </Box>
          )}
          <Button
            label="Avadakedavra"
            primary={true}
            onClick={() => (document.body.innerHTML = '')}
          />
        </Box>
      </Box>
    </>
  );
};
